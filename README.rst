docker-polarbytebot-gw2-submitter
=================================

* all actions to the database and all the actual logic is part of the `polarbytebot library <https://gitlab.com/networkjanitor/libpolarbytebot>`_
* this is a small microservice/dockercontainer which takes comments/submissions/etc out of their queues and submits them to reddit