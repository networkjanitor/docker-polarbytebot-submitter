import praw
import logging
import os
import sys
import configparser
import time
from libpolarbytebot import polarbytebot

__version__ = '0.0.1.dev0'
__owner = 'Xyooz'
__source_link = 'https://gitlab.com/networkjanitor/docker-polarbytebot-submitter'
__user_agent = f'polarbytebot-submitter/{__version__} by /u/{__owner}'
__signature = f'\n' \
              f'\n' \
              f'---\n' \
              f'^(Beep boop. This message was created by a bot. Please message /u/{__owner} if you have any ' \
              f'questions, suggestions or concerns.) [^Source ^Code]({__source_link})'
__subreddits = 'test' #'guildwars2+test+gw2economy'
__microservice_id = 'submitter'


def run():
    path_to_conf = os.path.abspath(os.path.dirname(sys.argv[0]))
    path_to_conf = os.path.join(path_to_conf, 'settings.conf')
    cfg = configparser.ConfigParser()
    cfg.read(path_to_conf)
    pbb = polarbytebot.Polarbytebot(signature=__signature, microservice_id=__microservice_id,
                                    oauth_client_id=cfg['oauth2']['client_id'],
                                    oauth_client_secret=cfg['oauth2']['client_secret'],
                                    oauth_redirect_uri=cfg['oauth2']['redirect_uri'],
                                    oauth_username=cfg['oauth2']['username'], praw_useragent=__user_agent,
                                    oauth_refresh_token=cfg.get('oauth2', 'refresh_token'),
                                    database_system=cfg.get('database', 'system'),
                                    database_username=cfg.get('database', 'username'),
                                    database_password=cfg.get('database', 'password'),
                                    database_host=cfg.get('database', 'host'),
                                    database_dbname=cfg.get('database', 'database'))

    while True:
        pbb.submit_submissions()
        pbb.submit_comments()
        pbb.submit_anetpool_edits()
        time.sleep(5)

if __name__ == '__main__':
    run()
